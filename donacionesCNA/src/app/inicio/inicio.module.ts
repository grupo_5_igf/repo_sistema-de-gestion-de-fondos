/*
*Nombre del módulo: inicio
*Dirección física: src/app/inicio/inicio.module.ts
*Objetivo: Definir el módulo inicio
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CookieModule } from 'ngx-cookie';
import { MaterializeModule } from 'angular2-materialize';

import { InicioRoutingModule } from './inicio-routing.module';
import { InicioComponent } from './inicio.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    MaterializeModule,
    CookieModule.forChild(),
    InicioRoutingModule
  ],
  declarations: [
    InicioComponent
  ],
  providers: [
  ]
})
export class InicioModule { }
