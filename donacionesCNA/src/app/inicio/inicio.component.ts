/*
*Nombre del componente: inicio
*Dirección física: src/app/componentes/inicio.component.ts
*Objetivo: Mostrar la pantalla de inicio del sitio.
**/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie';

declare var $: any;

@Component({
  selector: 'inicio',
  templateUrl: './inicio.component.html'
})
export class InicioComponent implements OnInit {
  rol: String;

  constructor(private router: Router, private cookieService: CookieService) { }

  ngOnInit() {
    $("#toogle_menu").sideNav({closeOnClick: true});

    // Recupera el rol de las cookies
    let u = this.cookieService.getObject('usuario');

    if(u)
      this.rol = u['rol'].nombre;

  }
}
