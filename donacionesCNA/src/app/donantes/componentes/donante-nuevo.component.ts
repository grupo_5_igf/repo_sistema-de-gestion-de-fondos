/*
*Nombre del componente: donante-nuevo
*Dirección física: src/app/donantes/componentes/donante-nuevo.component.ts
*Objetivo: Agregar un nuevo donante.
**/

import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";

import { DonantesService, Donante, Institucion} from './../servicios';

// import {  } from './../servicios';

declare var Materialize: any;

@Component({
  templateUrl: './donante-nuevo.component.html'
})
export class DonanteNuevoComponent implements OnInit {
  //Variables para el proceso de creación
  donante: Donante;
  institucion: Institucion;
  
  mensajeCreacion: boolean = false;
  mensajeError: string;

  modalCancel = new EventEmitter<string | MaterializeAction>();

  constructor(
    private donantesService: DonantesService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.donante = new Donante;
    this.institucion=new Institucion;
  }

  // Método: crear
  // Objetivo: Crear un nuevo donante
  crear() {
    // Mostrar mensajes
    this.mensajeCreacion = true;
    this.mensajeError = null;

    this.donante.institucion = this.institucion;
    this.donante.estado = "Activo";

    // Llamada al servicios
    this.donantesService.crear(this.donante).subscribe(
      message => {
        this.mensajeCreacion = false;
        Materialize.toast("Donante creado", 3000, 'toastSucess');
        this.limpiar();
      },
      error => {
        this.mensajeCreacion = false;
        this.mensajeError = "Error al crear el donante";
      }
    );
  }

  // Método para limpiar el formulario
  limpiar(){
    this.donante = new Donante;
    this.institucion = new Institucion;

  }

  // Métodos para el manejo de la ventana modal de confirmación de cancelación.
  openCancel() {
    this.modalCancel.emit({ action: "modal", params: ['open'] });
  }
  closeCancel() {
    this.modalCancel.emit({ action: "modal", params: ['close'] });
  }

  // Método: cancel
  // Objetivo: Cerrar la ventana modal y retornar a la vista anterior.
  cancel() {
    this.closeCancel();
    this.router.navigate(['/donantes']);
  }
}
