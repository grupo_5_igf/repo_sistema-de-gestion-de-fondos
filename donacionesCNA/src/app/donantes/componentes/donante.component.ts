/*
*Nombre del componente: donante
*Dirección física: src/app/donante/componentes/donante.component.ts
*Objetivo: Mostrar la información de un donante
**/
import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";
import { Subject } from 'rxjs/Rx';


import { DonantesService, Donante, Institucion, Donacion } from './../servicios';

declare var Materialize: any;


@Component({
  templateUrl: './donante.component.html',
  styles: [`
    .modal-fixed-footer{
      max-height: 350px;
      max-width: 400px;
    }
  `]
})

export class DonanteComponent implements OnInit {
  id: number;

  //Variables para el proceso de edición
  edicion: boolean = false;
  donante: Donante;
  institucion: Institucion;

  //Variables para agregar donación
  donacionNueva: Donacion;

  modalDonacion = new EventEmitter<string | MaterializeAction>();

  constructor(
    private donantesService:DonantesService,
    private route: ActivatedRoute,
    private router: Router)
  { }

  ngOnInit() {
    this.donacionNueva = new Donacion;

    // Obtiene el id del donante
    this.id = this.route.snapshot.params['id'];

    this.inicializar();
  }
  // Método inicializar
  inicializar(){
    // Llama al servicio
    this.donantesService.verDonante(this.id).subscribe(
      donante => {
        this.donante = donante;
      }
    );
  }

  // Método para habilitar formulario de edición
  habilitar(){
    this.edicion = true;
  }
  //Método para cancelar edición
  cancelar(){
    this.edicion = false;
  }

  // Método: editar
  // Objetivo: Modificar la información de usuario
  editar(){
    // Consumir servicio de usuario
    this.donantesService.modificar(this.donante).subscribe(
      message => {
        Materialize.toast("Datos modificados", 3000, 'toastSuccess');
        this.edicion = false;
      },
      error => {
        Materialize.toast("Error al modificar donante", 3000);
      }
    )
  }

  // Método: agregar
  // Objetivo: agregar una nueva donación
  agregar(form: any){

    this.donacionNueva.fecha = (new Date()).toLocaleDateString();

    this.donantesService.nuevaDonacion(this.donacionNueva, this.donante.id).subscribe(
      id => {
        this.donacionNueva.id = id;
        form.reset();
        this.donacionNueva = new Donacion;
        this.inicializar();
        Materialize.toast("Donación agregada con éxito", 3000);
      },
      error => {
        Materialize.toast("Error al agregar donación", 3000);
      }
    )
  }

  // Métodos para el manejo de la ventana modal para agregar items
  openDonacion() {
    this.modalDonacion.emit({ action: "modal", params: ['open'] });
  }
  closeDonacion() {
    this.modalDonacion.emit({ action: "modal", params: ['close'] });
  }
}
