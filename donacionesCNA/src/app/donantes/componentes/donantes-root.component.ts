/*
*Nombre del componente: donantes-root
*Dirección física: src/app/donantes/componentes/donantes-root.component.ts
*Objetivo: Formar la estructura base para los componentes del módulo Donantes
**/

import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  templateUrl: './donantes-root.component.html',
  styles: [`
    h5{
      font-weight: bold;
      margin: 20px 0 20px 0;
    }
  `]
})
export class DonantesRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $("#toogle_menu").sideNav({closeOnClick: true});
  }

}
