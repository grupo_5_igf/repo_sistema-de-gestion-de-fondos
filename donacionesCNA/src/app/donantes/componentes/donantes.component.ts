import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Rx';

import { DonantesService, Donante, Institucion } from './../servicios';

declare var $: any;

@Component({
  templateUrl: './donantes.component.html'
})
export class DonantesComponent implements OnInit {
donantes: Donante[];

dtOptions: DataTables.Settings = {};
dtTrigger: Subject<any> = new Subject();
  constructor(private donantesService:DonantesService,private router: Router) {
  //opciones de DataTable
  this.dtOptions = {
    pageLength: 10,
    pagingType: 'simple_numbers',
    order: [[0, "asc"]],
    language: {
      "emptyTable": "Sin registros disponibles en la tabla",
      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
      "infoEmpty": "",
      "infoFiltered": "(filtrados de _MAX_ totales )",
      "lengthMenu": " ",
      "search": "Buscar:",
      "zeroRecords": "Búsqueda sin resultados",
      "paginate": {
        "first": "Primero",
        "last": "Último",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }
  };
}


  ngOnInit() {

    // Llama al servicio
    this.donantesService.obtenerDonantes().subscribe(
      donantes => {
        // Asigna los usuarios y refresca la tabla
        this.donantes = donantes;
        this.dtTrigger.next();
      }
    );
  }

}
