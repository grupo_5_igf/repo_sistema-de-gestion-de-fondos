export { DonantesRootComponent } from './donantes-root.component';
export { DonanteComponent } from './donante.component';
export { DonanteNuevoComponent } from './donante-nuevo.component';
export { DonantesComponent } from './donantes.component';
