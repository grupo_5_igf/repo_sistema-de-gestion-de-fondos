/*
*Nombre del servicio: donantes
*Dirección física: src/app/donantes/servicios/donantes.service.ts
*Objetivo: Proveer los servicios al módulo donantes
**/

import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { CookieService } from 'ngx-cookie';
import { Donante, Institucion, Donacion } from './';

@Injectable()
export class DonantesService {
  baseUrl: string;
  busUrl: string;
  headers: Headers;

  constructor(private http: Http, private cookieService: CookieService) {
    this.busUrl = environment.busURL;
    this.baseUrl = environment.donacionesURL;
    this.headers = new Headers({ 'Content-Type': 'application/json', 'X-AUTH-TOKEN': this.cookieService.get('token') });
  }

  // Método: obtenerDonantes
  // Objetivo: Obtener el listado de donantes
  obtenerDonantes(): Observable<Donante[]> {
    // let url = this.busUrl + 'donantes';
    let url = 'http://localhost:8081/donantes';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let donantes = new Array<Donante>();

        r.forEach((_donante) => {
          let donante = new Donante;
          let rp = _donante['idInstitucion'];

          donante.id = _donante['idPersona'];
          donante.nombre = _donante['nombre'];
          donante.apellido = _donante['apellido'];
          donante.dui = _donante['dui'];
          donante.nit = _donante['nit'];
          donante.telefono = _donante['telefono'];
          donante.cargo = _donante['cargo'];
          donante.correo = _donante['correoElectronico'];
          donante.estado = _donante['estado'];

          if (rp != null) {
            let institucion = new Institucion;
            institucion.id = rp['idInstitucion'];
            institucion.nombre = rp['nombreInstitucion'];
            donante.institucion = institucion;
          }

          donantes.push(donante);
        });
        console.log(donantes);
        return donantes;
      }
    );
  }

  // Método: crear
  // Objetivo: Crear un nuevo donante
  crear(donante: Donante): Observable<string> {
    let url = this.baseUrl + 'donante/new';
    // let url = 'http://localhost:8081/donante/nuevo';

    let q: any;
    // Mapeando la entrada
    if(donante.institucion){
      q = JSON.stringify({
        nombre: donante.nombre,
        apellido: donante.apellido,
        dui: donante.dui,
        nit: donante.nit,
        telefono: donante.telefono,
        cargo: donante.cargo,
        correo_electronico: donante.correo,
        estado: donante.estado,
        idInstitucion:{
          nombreInstitucion: donante.institucion.nombre,
          tipoInstitucion: donante.institucion.tipo,
          direccion: donante.institucion.direccion,
          correo: donante.institucion.correo
        }
      });
    } else {
      q = JSON.stringify({
        nombre: donante.nombre,
        apellido: donante.apellido,
        dui: donante.dui,
        nit: donante.nit,
        telefono: donante.telefono,
        cargo: donante.cargo,
        correo_electronico: donante.correo,
        estado: donante.estado
      });
    }

    console.log(q);
    // Realizando POST
    return this.http.post(url, q, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        console.log(r);
        return r;
      }
    );
  }

  // Método: verDonante
  // Objetivo: Obtener la información de un  donante
  verDonante(id: number): Observable<Donante> {
    // let url = this.baseUrl + 'donante/' + id;
    let url = 'http://localhost:8081/donante?id='+id;
    console.log(url);
    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();

        let rp = r['idInstitucion'];
        let rd = r['donaciones'];
        let donante = new Donante;
        let donaciones = new Array<Donacion>();

        donante.id = r['idPersona'];
        donante.nombre = r['nombre'];
        donante.apellido = r['apellido'];
        donante.dui = r['dui'];
        donante.nit = r['nit'];
        donante.telefono = r['telefono'];
        donante.cargo = r['cargo'];
        donante.correo = r['correoElectronico'];
        donante.estado = r['estado'];

        if (rp) {
          let institucion = new Institucion;
          institucion.id = rp['idInstitucion'];
          institucion.nombre = rp['nombreInstitucion'];
          institucion.direccion = rp['direccion'];
          institucion.correo = rp['correo'];
          institucion.tipo = rp['tipoInstitucion'];
          institucion.id_persona = rp['id_persona'];

          donante.institucion = institucion;
        }

        if(rd){
          rd.forEach((d) => {
            let donacion = new Donacion;

            donacion.id = d['idDonacion'];
            donacion.monto = d['monto'];
            donacion.fecha = d['fecha'];
            donacion.idPersona = d['idPersona'];
            donaciones.push(donacion);
          });
        }
        console.log(donaciones);
        donante.donaciones = donaciones;
        return donante;
      }
    );
  }

  // Método: modificar
  // Objetivo: Modificar información del donante
  modificar(donante: Donante): Observable<string> {
    let url = this.baseUrl + 'donante/' + donante.id + '/edit';
    if (!donante.institucion) {
      donante.institucion = null;
    }
    // Mapeando la entrada
    let q = JSON.stringify({
      id_persona: donante.id,
      nombre: donante.nombre,
      apellido: donante.apellido,
      dui: donante.dui,
      nit: donante.nit,
      telefono: donante.telefono,
      cargo: donante.cargo,
      correo_electronico: donante.correo,
      estado: donante.estado,
      id_institucion: donante.institucion

    }
    );

    return this.http.put(url, q, { headers: this.headers }).map(
      res => res.json()
    );
  }

  // Método: nuevaDonacion
  // Objetivo: agregar nueva donación del donante
  nuevaDonacion(donacion: Donacion, id:number): Observable<number>{
    let url = this.baseUrl + 'donacion/new';
    console.log(url);
    // Mapeando la entrada
    let q = JSON.stringify({
      monto: donacion.monto.toString(),
      fecha: donacion.fecha,
      idPersona: {
        idPersona: id
      }
    });
    console.log("JSON" + q);
    // Realizando POST
    return this.http.post(url, q, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        return r['idDonacion'];
      }
    );

  }
}
