/*
*Nombre de la clase: donante
*Dirección física: src/app/donantes/servicios/donante.ts
**/

import { Institucion, Donacion } from './';

export class Donante{
  id: number;
  nombre: string;
  apellido: string;
  dui: string;
  nit: string;
  telefono: string;
  cargo: string;
  correo: string;
  estado: string;
  institucion: Institucion;
  donaciones: Donacion[];
}
