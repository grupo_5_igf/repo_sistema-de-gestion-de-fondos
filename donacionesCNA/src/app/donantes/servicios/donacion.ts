/*
*Nombre de la clase: donacion
*Dirección física: src/app/donantes/servicios/donacion.ts
**/

export class Donacion{
  id: number;
  monto: string;
  fecha: string;
  idPersona: number;
}
