/*
*Nombre del módulo: donantes-routing
*Dirección física: src/app/donantes/donantes-routing.module.ts
*Objetivo: Definir las rutas del módulo Donantes
**/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DonantesRootComponent, DonantesComponent, DonanteNuevoComponent, DonanteComponent } from './componentes';
import { AppAuthGuard } from './../login'

const routes: Routes = [
  {
    path: 'donantes',
    component: DonantesRootComponent,
    children: [
      {
        path: '',
        component: DonantesComponent
      },
      {
        path: 'nuevo',
        component: DonanteNuevoComponent
      },
      {
        path: ':id',
        component: DonanteComponent
      }
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class DonantesRoutingModule { }
