/*
*Nombre del módulo: donantes
*Dirección física: src/app/donantes/donantes.module.ts
*Objetivo: Definir el módulo donantes
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CookieModule } from 'ngx-cookie';
import { MaterializeModule } from 'angular2-materialize';
import { DataTablesModule } from 'angular-datatables';


import { DonantesRoutingModule } from './donantes-routing.module';
import { DonantesRootComponent, DonantesComponent, DonanteNuevoComponent, DonanteComponent } from './componentes';
import { DonantesService } from './servicios';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    MaterializeModule,
    DataTablesModule,
    CookieModule.forChild(),
    DonantesRoutingModule
  ],
  declarations: [
    DonantesRootComponent,
    DonantesComponent,
    DonanteNuevoComponent,
    DonanteComponent
  ],
  providers: [
    DonantesService
  ]
})
export class DonantesModule { }
