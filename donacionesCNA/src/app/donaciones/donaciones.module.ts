/*
*Nombre del módulo: donaciones
*Dirección física: src/app/donaciones/donaciones.module.ts
*Objetivo: Definir el módulo donaciones
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CookieModule } from 'ngx-cookie';
import { MaterializeModule } from 'angular2-materialize';
import { DataTablesModule } from 'angular-datatables';

import { DonacionesRoutingModule } from './donaciones-routing.module';
import { DonacionComponent } from './donacion.component';
import { DonacionesService, ProgramasService } from './servicios';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    MaterializeModule,
    DataTablesModule,
    CookieModule.forChild(),
    DonacionesRoutingModule
  ],
  declarations: [
    DonacionComponent
  ],
  providers: [
    DonacionesService,
    ProgramasService
  ]
})
export class DonacionesModule { }
