/*
*Nombre del componente: donacion
*Dirección física: src/app/donaciones/componentes/donacion.component.ts
*Objetivo: Ver el detalle de una donación
**/

import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";
import { Subject } from 'rxjs/Rx';


import { DonacionesService, Donacion, Persona, Salida,
         ProgramasService, Programa } from './servicios';

declare var Materialize: any;
declare var $: any;

@Component({
  templateUrl: './donacion.component.html',
  styles: [`
    h5{
      font-weight: bold;
      margin: 20px 0 20px 0;
    }
    .modal-fixed-footer{
      max-height: 425px;
      max-width: 400px;
    }
    tfoot{
      font-weight: bold;
    }
  `]
})

export class DonacionComponent implements OnInit {
  id: number;
  donacion: Donacion;
  sobrante: number;

  // Variables para agregar salidas
  programas: Programa[];
  nuevaSalida: Salida;
  error: string;

  modalSalida = new EventEmitter<string | MaterializeAction>();

  constructor(
    private programasService: ProgramasService,
    private donacionesService: DonacionesService,
    private route: ActivatedRoute,
    private router: Router)
  { }

  ngOnInit() {
    $("#toogle_menu").sideNav({closeOnClick: true});
    // Inicializando variables
    this.nuevaSalida = new Salida;
    this.sobrante = 0;

    // Obtiene el id de la donación
    this.id = this.route.snapshot.params['id'];

    // Recuperar programas
    this.programasService.obtener().subscribe(
      programas => {
        this.programas = programas;
        this.obtenerDonacion();
      }
    );

  }
  // Obtener donacion luego de obtener programas
  obtenerDonacion(){
    // Llama al servicio
    this.donacionesService.ver(this.id).subscribe(
      donacion => {
        this.donacion = donacion;
        console.log(this.programas);
        this.sobrante = parseFloat(this.donacion.monto);
        // Obtener el nombre del programa asociado a la salida
        this.donacion.salidas.forEach((outflow) => {
          this.sobrante = this.sobrante - parseFloat(outflow.monto);
          let program: Programa = this.programas.find((programa) => {
            return programa.id == outflow.idPrograma;
          });
          outflow.programa = program.nombre;
        });
      }
    );
  }

  // Registrar salida de dinero a la donación
  salida(form: any){
    this.programas.forEach((programa) => {
      if (this.nuevaSalida.idPrograma == programa.id) {
        this.nuevaSalida.idPrograma = programa.id;
      }
    });

    if(parseFloat(this.nuevaSalida.monto) > this.sobrante){
      Materialize.toast("Solamente se pueden utilizar $"+this.sobrante, 3000);
    } else {
      this.donacionesService.registrarSalida(this.donacion, this.nuevaSalida).subscribe(
        message => {
          if (!message) Materialize.toast("Error! El programa ya ha sido financiado con esta donación.", 3000);
          else {
            // Materialize.toast("Salida registrada", 1000);
            this.donacion.salidas.push(this.nuevaSalida);
            this.obtenerDonacion();
            Materialize.toast("Salida registrada", 3000);
          }
        },
        error => {
          Materialize.toast("Error al registrar salida", 3000);
        }
      );
    }

    form.reset();
    this.nuevaSalida = new Salida;
  }

  // Métodos para el manejo de la ventana modal.
  openSalida() {
    this.error = "";
    this.modalSalida.emit({ action: "modal", params: ['open'] });
  }
  closeSalida() {
    this.modalSalida.emit({ action: "modal", params: ['close'] });
  }
}
