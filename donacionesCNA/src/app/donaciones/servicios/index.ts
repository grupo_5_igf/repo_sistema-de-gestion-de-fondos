export { DonacionesService } from './donaciones.service';
export { Donacion } from './donacion';
export { Salida } from './salida';
export { Institucion } from './institucion';
export { Persona } from './persona';

export { ProgramasService } from './programas.service';
export { Programa } from './programa';
