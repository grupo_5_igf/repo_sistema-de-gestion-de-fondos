/*
*Nombre de la clase: institucion
*Dirección física: src/app/donaciones/servicios/institucion.ts
**/

export class Institucion{
  id: number;
  nombre: string;
  direccion: string;
  correo: string;
  tipo: string;
}
