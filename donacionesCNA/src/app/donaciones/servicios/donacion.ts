/*
*Nombre de la clase: donacion
*Dirección física: src/app/donantes/servicios/donacion.ts
**/

import { Persona, Salida } from './';

export class Donacion{
  id: number;
  monto: string;
  persona: Persona;
  salidas: Salida[];
}
