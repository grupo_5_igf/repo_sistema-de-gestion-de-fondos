/*
*Nombre de la clase: persona
*Dirección física: src/app/donaciones/servicios/persona.ts
**/

import { Institucion } from './';

export class Persona{
  id: number;
  nombre: string;
  apellido: string;
  dui: string;
  nit: string;
  telefono: string;
  cargo: string;
  correo: string;
  estado: string;
  institucion: Institucion;
}
