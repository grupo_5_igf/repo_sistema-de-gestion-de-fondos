/*
*Nombre de la clase: salida
*Dirección física: src/app/donaciones/servicios/salida.ts
**/

export class Salida{
  id: number;
  idPrograma: number;
  monto: string;
  programa: string;
}
