/*
*Nombre del servicio: donaciones
*Dirección física: src/app/donaciones/servicios/donaciones.service.ts
*Objetivo: Proveer los servicios al módulo donaciones
**/

import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { CookieService } from 'ngx-cookie';
import { Donacion, Persona, Salida, Institucion } from './';

@Injectable()
export class DonacionesService {
  baseUrl: string;
  busUrl: string;
  headers: Headers;

  constructor(private http: Http, private cookieService: CookieService) {
    this.baseUrl = environment.donacionesURL;
    this.busUrl = environment.busURL;
    this.headers = new Headers({ 'Content-Type': 'application/json', 'X-AUTH-TOKEN': this.cookieService.get('token') });
  }

  // Método: ver
  // Objetivo: ver el detalle de una donación
  ver(id: number): Observable<Donacion>
  {
    // let url = this.baseUrl + 'donacion/' + id;
    let url = this.busUrl + 'donacion?id=' + id;
    
    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();

        let rp = r['idPersona'];
        let rs = r['idSalida'];

        let donacion = new Donacion;
        let persona = new Persona;
        let salidas = new Array<Salida>();

        donacion.id = r['idDonacion'];
        donacion.monto = r['monto'];

        persona.id = rp['idPersona'];
        persona.nombre = rp['nombre'];
        persona.apellido = rp['apellido'];
        persona.estado = rp['estado'];

        if (rp['idInstitucion']) {
          let institucion = new Institucion;
          institucion.id = rp['idInstitucion'];
          institucion.nombre = rp['nombreInstitucion'];
          institucion.direccion = rp['direccion'];
          institucion.correo = rp['correo'];
          institucion.tipo = rp['tipoInstitucion'];

          persona.institucion = institucion;
        }

        donacion.persona = persona;

        if(rs){
          rs.forEach((s) => {
            let salida = new Salida;
            salida.id = s['idSalida'];
            salida.idPrograma = s['idProgramas'];
            salida.monto = s['montoSalida'];

            salidas.push(salida);
          });
        }

        donacion.salidas = salidas;

        return donacion;
      }
    );
  }

  // Método: registrarSalida
  // Objetivo: registrar una salida de dinero de la donación
  registrarSalida(donacion:Donacion, salida:Salida): Observable<boolean>{
    //Comprobar que el item no pertenece a la lista de items actuales
    let nuevo: boolean = true;

    donacion.salidas.forEach((s)=>{
      if(s.idPrograma == salida.idPrograma) {
        nuevo = false;
      }
    });

    if(nuevo){
      let url = this.baseUrl + 'donacion/'+donacion.id+'/addSalida';

      // Mapeando la entrada
      let q = JSON.stringify({ idProgramas: salida.idPrograma, montoSalida: salida.monto+""});

      return this.http.patch(url, q, { headers: this.headers }).map(
        // Mapeando salida
        (response: Response) => {
          return true;
        }
      );
    } else
        return Observable.of(false);

  }
}
