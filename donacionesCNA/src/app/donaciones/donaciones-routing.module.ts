/*
*Nombre del módulo: donaciones-routing
*Dirección física: src/app/donaciones/donaciones-routing.module.ts
*Objetivo: Definir las rutas del módulo Donaciones
**/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DonacionComponent } from './donacion.component';
import { AppAuthGuard } from './../login'

const routes: Routes = [
  {
    path: 'donaciones/:id',
    component: DonacionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class DonacionesRoutingModule { }
