/*
*Nombre de la clase: persona
*Dirección física: src/app/login/servicios/persona.ts
**/

export class Persona{
  nombre: string;
  apellido: string;
}
