/*
*Nombre del componente: app
*Dirección física: src/app/app.component.ts
*Objetivo: Componente principal
**/

import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie';
import { Persona } from './login/servicios';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  persona: Persona;
  nombreCompleto: string;



  constructor(private router: Router, private cookieService: CookieService) { }

  ngOnInit(){
    // Recupera el usuario de las cookies y coloca su correo en el nav.
    let u = this.cookieService.getObject('usuario');
    if(u){
      this.persona = u['persona'];
      this.nombreCompleto = this.persona['nombre'] + ' ' + this.persona['apellido'];
    }
  }

  // Método: cerrarSesión
  // Objetivo: Cierra la sesión del usuario al remover todos los cookies guardados.
  cerrarSesion() {
    this.cookieService.removeAll();
    // Redirige al login.
    window.location.href = "./login";
  }
}
