/*
*Nombre del componente: usuarios-root
*Dirección física: src/app/usuarios/componentes/usuarios-root.component.ts
*Objetivo: Formar la estructura base para los componentes del módulo Usuarios
**/

import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  templateUrl: './usuarios-root.component.html',
  styles: [`
    h5{
      font-weight: bold;
      margin: 20px 0 20px 0;
    }
  `]
})
export class UsuariosRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $("#toogle_menu").sideNav({closeOnClick: true});
  }

}
