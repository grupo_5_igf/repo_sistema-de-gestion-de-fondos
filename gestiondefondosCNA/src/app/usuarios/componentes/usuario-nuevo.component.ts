/*
*Nombre del componente: usuario-nuevo
*Dirección física: src/app/usuarios/componentes/usuario-nuevo.component.ts
*Objetivo: Agregar un nuevo usuario.
**/

import { Component, OnInit, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";

import { UsuariosService, EstadoCivil, Rol, Sexo, Usuario, Persona } from './../servicios';

declare var Materialize: any;

@Component({
  templateUrl: './usuario-nuevo.component.html'
})
export class UsuarioNuevoComponent implements OnInit {
  // Variables para controlar Selects
  estadosCiv: EstadoCivil[];
  estadosCivSelect: EstadoCivil;
  roles: Rol[];
  rolesSelect: Rol;
  sexos: Sexo[];
  sexosSelect: Sexo;

  //Variables para el proceso de creación
  usuario: Usuario;
  persona: Persona;
  estado: EstadoCivil;
  sexo: Sexo;
  rol: Rol;

  mensajeCreacion: boolean = false;
  mensajeError: string;

  modalCancel = new EventEmitter<string | MaterializeAction>();

  constructor(
    private usuariosService: UsuariosService,
    private route: ActivatedRoute,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.usuario = new Usuario;
    this.persona = new Persona;
    this.estado = new EstadoCivil;
    this.sexo = new Sexo;
    this.rol = new Rol;

    // Recuper sexos
    this.usuariosService.obtenerSexos().subscribe(
      sexos => {
        this.sexos = sexos;

        // Para actualizar la vista del select
        this.cd.detectChanges();
        this.sexosSelect = this.sexos[0];
        this.selectSexo();
      }
    );

    // Recuperar estados civiles
    this.usuariosService.obtenerEstadosCiviles().subscribe(
      estadosCiv => {
        this.estadosCiv = estadosCiv;
        // Para actualizar la vista del select
        this.cd.detectChanges();
        this.estadosCivSelect = this.estadosCiv[0];
        this.selectEstadoCiv();
      }
    );

    // Recuper roles
    this.usuariosService.obtenerRoles().subscribe(
      roles => {
        this.roles = roles;
        // Para actualizar la vista del select
        this.cd.detectChanges();
        this.rolesSelect = this.roles[0];
        this.selectRol();
      }
    );
  }

  // Métodos para manejar los cambios en los select y asignarlos al objeto correspondiente
  selectEstadoCiv() {
    this.estado.id = Number($("#estado").val());
    this.estado.nombre = $("#estado option:selected").html();

    this.persona.estadoCivil = this.estado;
  }

  selectSexo() {
    this.sexo.id = Number($("#sexo").val());
    this.sexo.nombre = $("#sexo option:selected").html();

    this.persona.sexo = this.sexo;
  }

  selectRol() {
    this.rol.id = Number($("#rol").val());
    this.rol.nombre = $("#rol option:selected").html();

    this.usuario.rol = this.rol;
  }

  // Método: crear
  // Objetivo: Crear un nuevo usuario
  crear() {
    // Mostrar mensajes
    this.mensajeCreacion = true;
    this.mensajeError = null;

    this.usuario.persona = this.persona;
    this.usuario.estado = "Activo";

    // Llamada al servicios
    this.usuariosService.crear(this.usuario).subscribe(
      message => {
        this.mensajeCreacion = false;
        Materialize.toast("Usuario creado", 3000, 'toastSucess');
        this.limpiar();
      },
      error => {
        this.mensajeCreacion = false;
        this.mensajeError = "Error al crear el usuario";
      }
    );
  }

  // Método para limpiar el formulario
  limpiar(){
    this.usuario = new Usuario;
    this.persona = new Persona;
    this.estado = new EstadoCivil;
    this.sexo = new Sexo;
    this.rol = new Rol;
  }
  // Métodos para el manejo de la ventana modal de confirmación de cancelación.
  openCancel() {
    this.modalCancel.emit({ action: "modal", params: ['open'] });
  }
  closeCancel() {
    this.modalCancel.emit({ action: "modal", params: ['close'] });
  }

  // Método: cancel
  // Objetivo: Cerrar la ventana modal y retornar a la vista anterior.
  cancel() {
    this.closeCancel();
    this.router.navigate(['/usuarios']);
  }
}
