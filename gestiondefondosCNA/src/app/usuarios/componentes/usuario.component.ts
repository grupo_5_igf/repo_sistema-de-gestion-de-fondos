/*
*Nombre del componente: usuario
*Dirección física: src/app/usuario/componentes/usuario.component.ts
*Objetivo: Mostrar la información de un usuario
**/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Rx';

import { UsuariosService, EstadoCivil, Rol, Sexo, Usuario, Persona } from './../servicios';

declare var Materialize: any;

@Component({
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {

  // Variables para controlar Selects
  estadosCiv: EstadoCivil[];
  estadosCivSelect: EstadoCivil;
  roles: Rol[];
  rolesSelect: Rol;
  sexos: Sexo[];
  sexosSelect: Sexo;

  //Variables para el proceso de edición
  edicion: boolean = false;
  usuario: Usuario;
  persona: Persona;
  estado: EstadoCivil;
  sexo: Sexo;
  rol: Rol;

  constructor(
    private usuariosService:UsuariosService,
    private route: ActivatedRoute,
    private router: Router)
  { }

  ngOnInit() {
    // Obtiene el id del usuario
    let id = this.route.snapshot.params['id'];

    // Llama al servicio
    this.usuariosService.verUsuario(id).subscribe(
      usuario => {
        this.usuario = usuario;
        this.persona = this.usuario.persona;
      }
    );
  }

  // Método para habilitar formulario de edición
  habilitar(){
    this.edicion = true;
  }
  //Método para cancelar edición
  cancelar(){
    this.edicion = false;
  }

  // Método: editar
  // Objetivo: Modificar la información de usuario
  editar(){
    // Consumir servicio de usuario
    this.usuariosService.modificar(this.usuario).subscribe(
      message => {
        Materialize.toast("Datos modificados", 3000, 'toastSuccess');
        this.edicion = false;
      },
      error => {
        Materialize.toast("Error al modificar usuario", 3000);
      }
    )
  }
}
