/*
*Nombre del módulo: usuarios
*Dirección física: src/app/usuarios/usuarios.module.ts
*Objetivo: Definir el módulo usuarios
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CookieModule } from 'ngx-cookie';
import { MaterializeModule } from 'angular2-materialize';
import { DataTablesModule } from 'angular-datatables';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosRootComponent, UsuariosComponent, UsuarioNuevoComponent, UsuarioComponent } from './componentes';
import { UsuariosService } from './servicios';
@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    MaterializeModule,
    DataTablesModule,
    CookieModule.forChild(),
    UsuariosRoutingModule
  ],
  declarations: [
    UsuariosRootComponent,
    UsuariosComponent,
    UsuarioNuevoComponent,
    UsuarioComponent
  ],
  providers: [
    UsuariosService
  ]
})
export class UsuariosModule { }
