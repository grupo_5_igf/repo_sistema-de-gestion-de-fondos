/*
*Nombre de la clase: usuario
*Dirección física: src/app/usuarios/servicios/usuario.ts
**/

import { Rol, Persona } from './';

export class Usuario{
  id: number;
  correo: string;
  contra: string;
  estado: string;
  rol: Rol;
  persona: Persona;
}
