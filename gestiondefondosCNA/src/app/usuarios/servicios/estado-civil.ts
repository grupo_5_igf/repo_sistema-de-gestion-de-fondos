/*
*Nombre de la clase: estadoCivil
*Dirección física: src/app/usuarios/servicios/estado-civil.ts
**/

export class EstadoCivil{
  id: number;
  nombre: string;
}
