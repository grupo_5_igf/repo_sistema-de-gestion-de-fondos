/*
*Nombre de la clase: persona
*Dirección física: src/app/usuarios/servicios/persona.ts
**/
import{ EstadoCivil, Sexo } from './';

export class Persona{
  nombre: string;
  apellido: string;
  dui: string;
  nit: string;
  direccion: string;
  telefono: string;
  estadoCivil: EstadoCivil;
  sexo: Sexo;
}
