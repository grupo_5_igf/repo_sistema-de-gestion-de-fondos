/*
*Nombre del servicio: usuarios
*Dirección física: src/app/usuarios/servicios/usuarios.service.ts
*Objetivo: Proveer los servicios al módulo usuarios
**/

import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { CookieService } from 'ngx-cookie';
import { EstadoCivil, Rol, Sexo, Persona, Usuario } from './';

@Injectable()
export class UsuariosService {
  baseUrl: string;
  busUrl: string;
  headers: Headers;

  constructor(private http: Http, private cookieService: CookieService) {
    this.baseUrl = environment.apiUsuariosURL;
    this.busUrl = environment.busURL;

    this.headers = new Headers({ 'Content-Type': 'application/json', 'X-AUTH-TOKEN': this.cookieService.get('token') });
  }

  // Método: obtenerEstadosCiviles
  // Objetivo: Obtener el listado de estados civiles
  obtenerEstadosCiviles(): Observable<EstadoCivil[]> {
    // let url = this.baseUrl + 'estadocivil/';
    let url = this.busUrl + 'estadosciviles';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida

      (response: Response) => {
        let r = response.json();
        let estadosCiv = new Array<EstadoCivil>();

        r.forEach((_estadoc) => {
          let estadoc = new EstadoCivil;

          estadoc.id = _estadoc['idEstadoCivil'];
          estadoc.nombre = _estadoc['nombreEstadoCivil'];

          estadosCiv.push(estadoc);
        });

        return estadosCiv;
      }
    );
  }

  // Método: obtenerRoles
  // Objetivo: Obtener el listado de roles disponibles
  obtenerRoles(): Observable<Rol[]> {
    // let url = this.baseUrl + 'rol/';
    let url = this.busUrl + 'roles';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let roles = new Array<Rol>();

        r.forEach((_rol) => {
          let rol = new Rol;
          rol.id = _rol['idRol'];
          rol.nombre = _rol['nombreRol'];
          console.log(rol);
          roles.push(rol);
        });

        return roles;
      }
    );
  }

  // Método: obtenerSexos
  // Objetivo: Obtener el listado de sexos disponibles
  obtenerSexos(): Observable<Rol[]> {
    // let url = this.baseUrl + 'sexo/';
    let url = this.busUrl + 'sexos';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let sexos = new Array<Sexo>();

        r.forEach((_sexo) => {
          let sexo = new Sexo;

          sexo.id = _sexo['idSexo'];
          sexo.nombre = _sexo['nombreSexo'];

          sexos.push(sexo);
        });

        return sexos;
      }
    );
  }

  // Método: crear
  // Objetivo: Crear un nuevo usuario
  crear(usuario: Usuario): Observable<string> {
    let url = this.baseUrl + 'usuario/new';

    // Mapeando la entrada
    let q = JSON.stringify({
      correo: usuario.correo,
      contrasenia: usuario.contra,
      estado: usuario.estado,
      id_rol: {
        id_rol: usuario.rol.id,
        nombre_rol: usuario.rol.nombre
      },
      id_persona: {
        nombre: usuario.persona.nombre,
        apellido: usuario.persona.apellido,
        dui: usuario.persona.dui,
        nit: usuario.persona.nit,
        direccion: usuario.persona.direccion,
        telefono: usuario.persona.telefono,
        id_estado_civil: {
          id_estado_civil: usuario.persona.estadoCivil.id,
          nombre_estado_civil: usuario.persona.estadoCivil.nombre
        },
        id_sexo: {
          id_sexo: usuario.persona.sexo.id,
          nombre_sexo: usuario.persona.sexo.nombre
        }
      }
    });

    // Realizando POST
    return this.http.post(url, q, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        return r;
      }
    );
  }

  // Método: obtenerUsuarios
  // Objetivo: Obtener el listado de usuarios
  obtenerUsuarios(): Observable<Usuario[]> {
    // let url = this.baseUrl + 'usuario';
    let url = this.busUrl + 'usuarios';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let usuarios = new Array<Usuario>();

        r.forEach((_usuario) => {
          let usuario = new Usuario;
          let rp = _usuario['id_persona'];
          let rr = _usuario['id_rol'];

          usuario.id = _usuario['id_usuario'];
          usuario.correo = _usuario['correo'];
          let persona = new Persona;
          persona.nombre = rp['nombre'];
          persona.apellido = rp['apellido'];
          usuario.persona = persona;
          let rol = new Rol;
          rol.nombre = rr['nombre_rol'];
          usuario.rol = rol;

          usuarios.push(usuario);
        });

        return usuarios;
      }
    );
  }

  // Método: verUsuario
  // Objetivo: Obtener la información de un  usuario
  verUsuario(id: number): Observable<Usuario>{
    let url = this.baseUrl + 'usuario/' + id;
    // let url = this.busUrl + 'usuario?id=' + id;

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let usuarios = new Array<Usuario>();

          let usuario = new Usuario;


          usuario.id = r['id_usuario'];
          usuario.correo = r['correo'];
          usuario.estado = r['estado'];
          usuario.contra = r['contrasenia'];

          let persona = new Persona;
          let rp = r['id_persona'];
          persona.nombre = rp['nombre'];
          persona.apellido = rp['apellido'];
          persona.dui = rp['dui'];
          persona.nit = rp['nit'];
          persona.telefono = rp['telefono'];
          persona.direccion = rp['direccion'];

          let estadoCiv = new EstadoCivil;
          let rpE = rp['id_estado_civil'];
          estadoCiv.id = rpE['id_estado_civil'];
          estadoCiv.nombre = rpE['nombre_estado_civil'];
          persona.estadoCivil = estadoCiv;

          let sexo = new Sexo;
          let rpS = rp['id_sexo'];
          sexo.id = rpS['id_sexo'];
          sexo.nombre = rpS['nombre_sexo'];
          persona.sexo = sexo;

          usuario.persona = persona;

          let rol = new Rol;
          let rr = r['id_rol'];
          rol.id = rr['id_rol'];
          rol.nombre = rr['nombre_rol'];
          usuario.rol = rol;

          console.log(usuario);
        return usuario;
      }
    );
  }

  // Método: modificar
  // Objetivo: Modificar información del usuario
  modificar(usuario: Usuario): Observable<string>{
    let url = this.baseUrl + 'usuario/' + usuario.id + '/edit';

    // Mapeando la entrada
    let q = JSON.stringify({
      correo: usuario.correo,
      contrasenia: usuario.contra,
      estado: usuario.estado,
      id_rol: {
        id_rol: usuario.rol.id,
        nombre_rol: usuario.rol.nombre
      },
      id_persona: {
        nombre: usuario.persona.nombre,
        apellido: usuario.persona.apellido,
        dui: usuario.persona.dui,
        nit: usuario.persona.nit,
        direccion: usuario.persona.direccion,
        telefono: usuario.persona.telefono,
        id_estado_civil: {
          id_estado_civil: usuario.persona.estadoCivil.id,
          nombre_estado_civil: usuario.persona.estadoCivil.nombre
        },
        id_sexo: {
          id_sexo: usuario.persona.sexo.id,
          nombre_sexo: usuario.persona.sexo.nombre
        }
      }
    });

    return this.http.put(url, q, { headers: this.headers }).map(
      res => res.json()
    );
  }

}
