/*
*Nombre de la clase: rol
*Dirección física: src/app/usuarios/servicios/rol.ts
**/

export class Rol{
  id: number;
  nombre: string;
}
