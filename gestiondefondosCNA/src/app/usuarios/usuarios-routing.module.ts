/*
*Nombre del módulo: usuarios-routing
*Dirección física: src/app/usuarios/usuarios-routing.module.ts
*Objetivo: Definir las rutas del módulo Usuarios
**/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuariosRootComponent, UsuariosComponent, UsuarioNuevoComponent, UsuarioComponent } from './componentes';
import { AppAuthGuard } from './../login'

const routes: Routes = [
  {
    path: 'usuarios',
    component: UsuariosRootComponent,
    children: [
      {
        path: '',
        component: UsuariosComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Administrador"}
      },
      {
        path: 'nuevo',
        component: UsuarioNuevoComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Administrador"}
      },
      {
        path: ':id',
        component: UsuarioComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Administrador"}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class UsuariosRoutingModule { }
