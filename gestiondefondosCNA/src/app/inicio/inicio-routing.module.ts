/*
*Nombre del módulo: inicio-routing
*Dirección física: src/app/inicio/inicio-routing.module.ts
*Objetivo: Definir las rutas del módulo Inicio
**/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './inicio.component';
import { AppAuthGuard } from './../login'

const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent,
    canActivate: [AppAuthGuard],
    data: {puesto: "*"}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class InicioRoutingModule { }
