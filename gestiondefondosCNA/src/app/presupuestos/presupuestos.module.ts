/*
*Nombre del módulo: presupuestos
*Dirección física: src/app/presupuestos/presupuestos.module.ts
*Objetivo: Definir el módulo presupuestos
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CookieModule } from 'ngx-cookie';
import { MaterializeModule } from 'angular2-materialize';
import { DataTablesModule } from 'angular-datatables';

import { PresupuestosRoutingModule } from './presupuestos-routing.module';
import { PresupuestosRootComponent, PresupuestosComponent, PresupuestoNuevoComponent, PresupuestoComponent } from './componentes';
import { PresupuestosService, ProgramasService } from './servicios';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    MaterializeModule,
    DataTablesModule,
    CookieModule.forChild(),
    PresupuestosRoutingModule
  ],
  declarations: [
    PresupuestosRootComponent,
    PresupuestosComponent,
    PresupuestoNuevoComponent,
    PresupuestoComponent
  ],
  providers: [
    PresupuestosService,
    ProgramasService
  ]
})
export class PresupuestosModule { }
