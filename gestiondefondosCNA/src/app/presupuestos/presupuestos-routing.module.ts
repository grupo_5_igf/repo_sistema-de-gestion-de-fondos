/*
*Nombre del módulo: presupuestos-routing
*Dirección física: src/app/presupuestos/presupuestos-routing.module.ts
*Objetivo: Definir las rutas del módulo Presupuestos
**/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresupuestosRootComponent, PresupuestosComponent, PresupuestoNuevoComponent, PresupuestoComponent } from './componentes';
import { AppAuthGuard } from './../login'

const routes: Routes = [
  {
    path: 'presupuestos',
    component: PresupuestosRootComponent,
    children: [
      {
        path: '',
        component: PresupuestosComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Presupuestador"}
      },
      {
        path: 'nuevo',
        component: PresupuestoNuevoComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Presupuestador"}
      },
      {
        path: ':id',
        component: PresupuestoComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Presupuestador"}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class PresupuestosRoutingModule { }
