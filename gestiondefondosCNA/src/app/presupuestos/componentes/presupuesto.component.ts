/*
*Nombre del componente: presupuesto
*Dirección física: src/app/presupuesto/componentes/presupuesto.component.ts
*Objetivo: Mostrar la información de un presupuesto
**/

import { Component, OnInit, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";
import { Subject } from 'rxjs/Rx';

import { PresupuestosService, Presupuesto, Programa, ItemPresupuesto, Gasto } from './../servicios';

declare var Materialize: any;
declare var $: any;

@Component({
  templateUrl: './presupuesto.component.html',
  styles: [`
    .accion:hover{
      color: red;
    }
    .accion:last-child{
      margin-left: 10px;
    }
    .modal-fixed-footer{
      max-height: 450px;
      max-width: 600px;
    }
  `]
})

export class PresupuestoComponent implements OnInit {
  presupuesto: Presupuesto;
  id: number;
  // Variables para el proceso de creación
  gastos: Gasto[];
  itemNuevo: ItemPresupuesto;

  itemEdit: ItemPresupuesto;
  subtotal: number;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  modalItem = new EventEmitter<string | MaterializeAction>();
  modalItemEdit = new EventEmitter<string | MaterializeAction>();

  constructor(
    private presupuestosService: PresupuestosService,
    private route: ActivatedRoute,
    private router: Router,
    private cd: ChangeDetectorRef
  ) {
    // Opciones de datatable
    this.dtOptions = {
      pageLength: 10,
      pagingType: 'simple_numbers',
      lengthMenu: [10, 15, 20],
      searching: false,
      language: {
        "emptyTable": "Sin registros disponibles en la tabla",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
        "infoEmpty": "",
        "infoFiltered": "(filtrados de _MAX_ totales )",
        "lengthMenu": " ",
        "search": "Buscar:",
        "zeroRecords": "Búsqueda sin resultados",
        "paginate": {
          "first": "Primero",
          "last": "Último",
          "next": "Siguiente",
          "previous": "Anterior"
        }
      }
    };
  }

  ngOnInit() {
    this.itemNuevo = new ItemPresupuesto;
    this.itemNuevo.gasto = new Gasto;

    this.itemEdit = new ItemPresupuesto;
    this.itemEdit.gasto = new Gasto;

    // Obtiene el id del usuario
    this.id = this.route.snapshot.params['id'];

    this.inicializar();

    // Recuper gastos
    this.presupuestosService.obtenerGastos().subscribe(
      gastos => {
        this.gastos = gastos;
        this.itemNuevo.gasto.id = null;
      }
    );

  }//Fin de ngOnInit()

  // Método: inicializar     || Objetivo: inicializar la información
  inicializar(){
    // Llama al servicio
    this.presupuestosService.verPresupuesto(this.id).subscribe(
      presupuesto => {
        this.presupuesto = presupuesto;
        this.dtTrigger.next();
      }
    );
  }

  // Método: agregarItem  ||  Objetivo: agregar un nuevo item al presupuesto
  agregarItem(form: any)
  {
    // Llamada al servicios
    this.presupuestosService.agregarItem(this.presupuesto, this.itemNuevo).subscribe(
      message => {
        if (!message) Materialize.toast("'" + this.itemNuevo.gasto.nombre + "' se encuentra actualmente en el presupuesto.", 3000);
        else this.actualizarLista();
      }
    );

    form.reset();
  }

  verGasto(item: any)
  {
    this.gastos.forEach((gasto) => {
      if (item == gasto.id) {
        this.itemNuevo.gasto.nombre = gasto.nombre;
        this.itemNuevo.gasto.precio = gasto.precio;
      }
    });
  }

  verSubtotal()
  {
    this.itemNuevo.subtotal = this.itemNuevo.cantidad * this.itemNuevo.gasto.precio;
  }

  verSubTotal(item: any){
    this.itemEdit.subtotal = this.itemEdit.cantidad * this.itemEdit.gasto.precio;
  }

  actualizarLista()
  {
    // this.presupuesto.items.push(this.itemNuevo);
    this.presupuesto.total = this.presupuesto.total + this.itemNuevo.subtotal;
    this.actualizarPresupuesto();
    Materialize.toast("'" + this.itemNuevo.gasto.nombre + "' agregado al presupuesto", 3000, 'toastSuccess');
  }

  // Método: actualizar   ||  Objetivo: Actualizar la cantidad de un item
  actualizarItem(form: any)
  {
    // Llamada al servicios
    this.presupuestosService.actualizarItem(this.itemEdit).subscribe(
      message => {
        Materialize.toast(this.itemEdit.gasto.nombre + " actualizado.", 3000);
      }, error =>{
        Materialize.toast("Error al intentar actualizar " + this.itemEdit.gasto.nombre, 3000);
      }
    );

    this.presupuesto.total = this.presupuesto.total - this.subtotal + this.itemEdit.subtotal;
    this.actualizarPresupuesto();
    form.reset();
    this.closeItemEdit();
  }

  // Método: actualizarPresupuesto   || Objetivo: actualizar datos del presupuesto cuando sea necesario
  actualizarPresupuesto(){
    this.presupuesto.fecha = new Date();

    this.presupuestosService.actualizar(this.presupuesto).subscribe(
      message => {
        this.inicializar();
      }, error => {
        Materialize.toast("Error en la actualización del presupuesto", 3000);
      }
    );
  }

  // Método: eliminar   ||  Objetivo: Eliminar un item.
  eliminar(item: ItemPresupuesto)
  {
    this.presupuesto.total = this.presupuesto.total - item.subtotal;

    this.presupuestosService.eliminarItem(item).subscribe(
      message => {
        let i = this.presupuesto.items.indexOf(item);
        if(i > -1) this.presupuesto.items.splice(i, 1);
        Materialize.toast("Item eliminado", 3000, "toastSuccess");
      },
      error => {
        Materialize.toast("Error al eliminar item", 3000, "toastError");
      }
    );

    //Actualizar monto del presupuesto al eliminar un item
    this.actualizarPresupuesto();
  }

  // Métodos para el manejo de la ventana modal para agregar Items
  openItem() {
    this.modalItem.emit({ action: "modal", params: ['open'] });
  }
  closeItem() {
    this.modalItem.emit({ action: "modal", params: ['close'] });
  }

  // Métodos para el manejo de la ventana modal de edición
  openItemEdit(item: any) {
    this.subtotal = item.subtotal;
    this.itemEdit = item;
    this.modalItemEdit.emit({ action: "modal", params: ['open'] });
  }
  closeItemEdit() {
    this.itemEdit = new ItemPresupuesto;
    this.modalItemEdit.emit({ action: "modal", params: ['close'] });
  }
}
