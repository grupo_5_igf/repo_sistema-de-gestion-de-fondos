/*
*Nombre del componente: presupuestos-root
*Dirección física: src/app/presupuestos/componentes/presupuestos-root.component.ts
*Objetivo: Formar la estructura base para los componentes del módulo Presupuestos
**/

import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  templateUrl: './presupuestos-root.component.html',
  styles: [`
    h5{
      font-weight: bold;
      margin: 20px 0 20px 0;
    }
  `]
})
export class PresupuestosRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $("#toogle_menu").sideNav({closeOnClick: true});
  }

}
