/*
*Nombre del componente: presupuesto-nuevo
*Dirección física: src/app/presupuestos/componentes/presupuesto-nuevo.component.ts
*Objetivo: Agregar un nuevo presupuesto.
**/

import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";

import { PresupuestosService, ProgramasService, Programa, Presupuesto } from './../servicios';

declare var Materialize: any;

@Component({
  templateUrl: './presupuesto-nuevo.component.html'
})
export class PresupuestoNuevoComponent implements OnInit {
  // Variables de creación
  presupuesto: Presupuesto;

  // Variables para el select
  programas: Programa[];

  modalCancel = new EventEmitter<string | MaterializeAction>();

  constructor(
    private programasService: ProgramasService,
    private presupuestosService: PresupuestosService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.presupuesto = new Presupuesto;
    this.presupuesto.fecha = new Date();
    this.presupuesto.programa = new Programa;

    // Recuperar programas
    this.programasService.obtener().subscribe(
      programas => {
        this.programas = programas;
        console.log(this.programas);
      }
    );
  }

  // Crear Presupuesto
  guardar(){
    // Llamada al servicios
    this.presupuestosService.crear(this.presupuesto).subscribe(
      message => {
        Materialize.toast("Presupuesto creado", 3000, 'toastSucess');
        this.router.navigate(['/presupuestos/'+message]);
      },
      error => {
        Materialize.toast("No se pudo crear el presupuesto", 3000);
      }
    );
  }

  //Actualiza el objeto en cuando se selecciona una opción
  selectPrograma(p: any) {
    this.programas.forEach((programa) => {
      if (p == programa.id) {
        this.presupuesto.programa.id = programa.id;
        this.presupuesto.programa.nombre = programa.nombre;
        this.presupuesto.programa.fechaInicio = programa.fechaInicio;
        this.presupuesto.programa.duracion = programa.duracion;
      }
    });
  }

  // Métodos para el manejo de la ventana modal de confirmación de cancelación.
  openCancel() {
    this.modalCancel.emit({ action: "modal", params: ['open'] });
  }
  closeCancel() {
    this.modalCancel.emit({ action: "modal", params: ['close'] });
  }

  // Método: cancel
  // Objetivo: Cerrar la ventana modal y retornar a la vista anterior.
  cancel() {
    this.closeCancel();
    this.router.navigate(['/presupuestos']);
  }

}
