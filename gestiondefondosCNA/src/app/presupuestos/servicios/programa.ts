/*
*Nombre de la clase: programa
*Dirección física: src/app/presupuestos/servicios/programa.ts
**/

export class Programa{
  id: number;
  nombre: string;
  fechaInicio: string;
  duracion: number;
}
