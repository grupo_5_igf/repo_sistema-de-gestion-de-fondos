import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { CookieService } from 'ngx-cookie';
import { Presupuesto, ItemPresupuesto, Gasto, Programa } from './';

@Injectable()
export class PresupuestosService {
  baseUrl: string;
  busUrl: string;
  headers: Headers;

  constructor(private http: Http, private cookieService: CookieService) {
    this.baseUrl = environment.apiPresupuestosURL;
    this.busUrl = environment.busURL;
    this.headers = new Headers({ 'Content-Type': 'application/json', 'X-AUTH-TOKEN': this.cookieService.get('token') });
  }

  // Método: crear
  // Objetivo: crear un presupuesto
  crear(presupuesto: Presupuesto): Observable<number>
  {
    let url = this.baseUrl + "presupuesto/new";
    let fecha = presupuesto.fecha.toISOString();
    let _fecha = fecha.substring(0, 19) + "+00:00";

    // Mapeando la entrada
    let q = JSON.stringify({
      total: 0,
      fecha_creacion: _fecha,
      estado_presupuesto: true,
      id_programa:{
        id_programa: presupuesto.programa.id
      }
    });

    // Realizando POST
    return this.http.post(url, q, { headers: this.headers }).map(
      // Mapeando salida
      (response: Response) => {
        let r = response.json();
        return r['id_presupuesto'];
      }
    );
  }

  // Método: obtenerPresupuestos
  // Objetivo: Listar los pressupuestos disponibles
  obtenerPresupuestos(): Observable<Presupuesto[]>
  {
    // let url = this.baseUrl + 'presupuesto/';
    let url = this.busUrl + 'presupuestos';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let presupuestos = new Array<Presupuesto>();

        r.forEach((_presupuesto) => {
          let presupuesto = new Presupuesto;

          presupuesto.id = _presupuesto['id_presupuesto'];
          presupuesto.total = _presupuesto['total'];
          presupuesto.estado = _presupuesto['estadoPresupuesto'];
          presupuesto.fecha = _presupuesto['fecha_creacion'];

          let programa = new Programa;
          let rp = _presupuesto['id_programa'];
          programa.nombre = rp['nombre_programa'];

          presupuesto.programa = programa;

          presupuestos.push(presupuesto);
        });

        return presupuestos;
      }
    );
  }

  // Método: verPresupuesto
  // Objetivo: Ver el detalle de un presupuesto
  verPresupuesto(id: number): Observable<Presupuesto>
  {
    let url = this.baseUrl + 'presupuesto/' + id;
    // let url = this.busUrl + 'presupuesto?id=' + id;

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();

        let presupuesto = new Presupuesto;
        let rp = r['id_programa'];
        let programa = new Programa;
        let ri = r['itempresupuestos'];
        let items = new Array<ItemPresupuesto>();

        presupuesto.id = r['id_presupuesto'];
        presupuesto.fecha = r['fecha_creacion'];
        presupuesto.total = r['total'];
        presupuesto.estado = r['estado_presupuesto'];

        programa.id = rp['id_programa'];
        programa.nombre = rp['nombre_programa'];
        programa.fechaInicio = rp['fecha_inicio'];
        programa.duracion = rp['duracion'];
        presupuesto.programa = programa;

        ri.forEach((_item) => {
          let item = new ItemPresupuesto;
          item.id = _item['id_item'];
          item.cantidad = _item['cantidad'];
          item.subtotal = _item['subtotal'];

          let rg = _item['id_gasto'];
          let gasto = new Gasto;
          gasto.id = rg['id_gasto'];
          gasto.nombre = rg['nombre_gasto'];
          gasto.precio = rg['precio'];

          item.gasto = gasto;

          items.push(item);
        });
        presupuesto.items = items;

        return presupuesto;
      }
    );

  }

  // Método: actualizar
  // Objetivo: actualizar información del presupuesto
  actualizar(presupuesto: Presupuesto):Observable<string>
  {
    let url = this.baseUrl + 'presupuesto/' + presupuesto.id + '/edit';
    let fecha = presupuesto.fecha.toISOString();
    let _fecha = fecha.substring(0, 19) + "-06:00";

    // Mapeando la entrada
    let q = JSON.stringify({
      total: presupuesto.total,
      fecha_creacion: _fecha
      // estado_presupuesto: presupuesto.estado,
    });

    return this.http.put(url, q, { headers: this.headers }).map(
      // Mapeando salida
      (response: Response) => {
        let r = response.json();
        return r;
      }
    );
  }

  /**   ITEMS   **/

  // Método: obtenerGastos
  // Objetivo: Obtener el listado de gastos registrados para evitar duplicidad
  obtenerGastos(): Observable<Gasto[]>
  {
    // let url = this.baseUrl + 'gastos/';
    let url = this.busUrl + 'gastos';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let gastos = new Array<Gasto>();

        r.forEach((_gasto) => {
          let gasto = new Gasto;

          gasto.id = _gasto['idGasto'];
          gasto.nombre = _gasto['nombreGasto'];
          gasto.precio = _gasto['precio'];

          gastos.push(gasto);
        });

        return gastos;
      }
    );
  }

  // Método: agregarItem
  // Objetivo: agregar item al presupuesto
  agregarItem(presupuesto:Presupuesto, item:ItemPresupuesto): Observable<boolean>
  {
    //Comprobar que el item no pertenece a la lista de items actuales
    let nuevo: boolean = true;

    presupuesto.items.forEach((i)=>{

      if(i.gasto.id == item.gasto.id) {
        nuevo = false;
      }
    });

    if(nuevo){
      let url = this.baseUrl + "itempresupuesto/new";

      // Mapeando la entrada
      let q = JSON.stringify({
        cantidad: item.cantidad,
        subtotal: item.subtotal,
        id_gasto:{
          id_gasto: item.gasto.id
        },
        id_presupuesto:{
          id_presupuesto: presupuesto.id
        }
      });

      // Realizando POST
      return this.http.post(url, q, { headers: this.headers }).map(
        // Mapeando salida
        (response: Response) => {
          return true;
        }
      );
    }

    else
      return Observable.of(false);

  }

  // Método: actualizarItem
  // Objetivo: actualizar el item seleccionado de la tabla
  actualizarItem(item: ItemPresupuesto): Observable<string>{
    let url = this.baseUrl + "itempresupuesto/" + item.id + "/edit";

    // Mapeando la entrada
    let q = JSON.stringify({
      cantidad: item.cantidad,
      subtotal: item.subtotal
    });

    return this.http.put(url, q, { headers: this.headers }).map(
      // Mapeando salida
      (response: Response) => {
        let r = response.json();
        return r;
      }
    );
  }

  // Método: eliminarItem
  // Objetivo: Eliminar item del presupuesto
  eliminarItem(item: ItemPresupuesto): Observable<string>
  {
    let url = this.baseUrl + "itempresupuesto/"+ item.id;

    // Realizando POST
    return this.http.delete(url, { headers: this.headers }).map(
      // Mapeando salida
      (response: Response) => {
        let r = response.json();
        return r['message'];
      }
    );
  }

}
