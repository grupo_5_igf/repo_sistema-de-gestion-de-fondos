/*
*Nombre de la clase: gasto
*Dirección física: src/app/presupuestos/servicios/gasto.ts
**/

export class Gasto{
  id: number;
  nombre: string;
  precio: number;
}
