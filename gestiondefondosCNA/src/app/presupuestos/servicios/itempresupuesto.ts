/*
*Nombre de la clase: itempresupuesto
*Dirección física: src/app/presupuestos/servicios/itempresupuesto.ts
**/

import { Gasto, Presupuesto } from './';

export class ItemPresupuesto{
  id: number;
  cantidad: number;
  subtotal: number;
  gasto: Gasto;
}
