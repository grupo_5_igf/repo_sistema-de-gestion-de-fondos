/*
*Nombre de la clase: presupuesto
*Dirección física: src/app/presupuestos/servicios/presupuesto.ts
**/

import { ItemPresupuesto, Programa } from './';

export class Presupuesto{
  id: number;
  total: number;
  fecha: Date;
  estado: boolean;
  programa: Programa;
  items: ItemPresupuesto[];
}
