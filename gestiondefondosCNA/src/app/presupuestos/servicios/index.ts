export { PresupuestosService } from './presupuestos.service';
export { ProgramasService } from './programas.service';
export { Gasto } from './gasto';
export { Presupuesto } from './presupuesto';
export { ItemPresupuesto } from './itempresupuesto';
export { Programa } from './programa';
