import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { CookieService } from 'ngx-cookie';
import { Programa } from './';

@Injectable()
export class ProgramasService {
  baseUrl: string;
  busUrl: string;
  headers: Headers;

  constructor(private http: Http, private cookieService: CookieService) {
    this.baseUrl = environment.apiPresupuestosURL;
    this.busUrl = environment.busURL;
    this.headers = new Headers({ 'Content-Type': 'application/json', 'X-AUTH-TOKEN': this.cookieService.get('token') });
  }

  // Método: obtener
  // Objetivo: obtener listado programas disponibles
  obtener(): Observable<Programa[]>{
    // let url = this.baseUrl + 'programa/';
    let url = this.busUrl + 'programas';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let programas = new Array<Programa>();

        r.forEach((_programa) => {
          let programa = new Programa;
          let rf = _programa['fechaInicio'];

          programa.id = _programa['id_programa'];
          programa.nombre = _programa['nombre_programa'];
          programa.duracion = _programa['duracion'];

          if(rf)
            programa.fechaInicio = rf['date'];

          programas.push(programa);
        });

        return programas;
      }
    );
  }
}
