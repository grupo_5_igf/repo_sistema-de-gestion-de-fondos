/*
*Nombre del servicio: auth
*Dirección: /src/app/login/servicios/auth.service.ts
*Objetivo: Proveer los servicios al módulo de login
*/

import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { Usuario, Persona, Rol } from './'

@Injectable()
export class AuthService {
  baseUrl: string;
  headers: Headers;

  constructor(private http: Http) {
    this.baseUrl = environment.apiUsuariosURL;
    this.headers = new Headers({ 'Content-Type': 'application/json'});
  }

  // Método: logueo
  // Objetivo: permite a los usuarios loguearse.
  logueo(correo: string, contra: string): Observable<any> {
    let url = this.baseUrl + 'login';
    let q = JSON.stringify({ correo: correo, contrasenia: contra });

    return this.http.post(url, q, { headers: this.headers }).map(
      (response: Response) => {
        let r = response.json();
        return {usuario: this.mapearUsuario(r['usuario']), token: r['token']};
      }
    );
  }

  // Método privado: mapearUsuario
  // Objetivo: convertir un json de respuesta en un objeto de Usuario.
  private mapearUsuario(r: any): Usuario{
    let usuario = new Usuario;
    usuario.id = r['id_usuario'];
    usuario.correo = r['correo'];

    let persona = new Persona;
    let rp = r['id_persona'];
    persona.nombre = rp['nombre'];
    persona.apellido = rp['apellido'];

    let rol = new Rol;
    let rr = r['id_rol'];
    rol.nombre = rr['nombre_rol'];
    usuario.rol = rol;

    usuario.persona = persona;

    return usuario;
  }

}
