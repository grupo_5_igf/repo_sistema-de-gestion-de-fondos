/*
*Nombre de la clase: rol
*Dirección física: src/app/login/servicios/rol.ts
**/

export class Rol{
  id: number;
  nombre: string;
}
