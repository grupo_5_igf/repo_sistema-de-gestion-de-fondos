/*
*Nombre de la clase: usuario
*Dirección: /src/app/login/servicios/usuario.ts
*/

import { Persona, Rol } from './';

export class Usuario {
  id: number;
  correo: string;
  persona: Persona;
  rol: Rol;
}
