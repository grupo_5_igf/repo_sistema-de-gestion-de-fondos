/*
*Nombre del componente: programas-root
*Dirección física: src/app/programas/componentes/programas-root.component.ts
*Objetivo: Formar la estructura base para los componentes del módulo Programas
**/

import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  templateUrl: './programas-root.component.html',
  styles: [`
    h5{
      font-weight: bold;
      margin: 20px 0 20px 0;
    }
  `]
})
export class ProgramasRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $("#toogle_menu").sideNav({closeOnClick: true});
  }

}
