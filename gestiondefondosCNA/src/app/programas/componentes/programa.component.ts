/*
*Nombre del componente: programa
*Dirección física: src/app/programas/componentes/programa.component.ts
*Objetivo: Ver el detalle de un programa
**/

import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";
import { Subject } from 'rxjs/Rx';

import { ProgramasService, TalleresService, Programa, Taller } from './../servicios';

declare var Materialize: any;
declare var $: any;

@Component({
  templateUrl: './programa.component.html',
  styles: [`
    .modal-fixed-footer{
      max-height: 400px;
      max-width: 400px;
    }
  `]
})
export class ProgramaComponent implements OnInit {
  id: number;
  programa: Programa;

  //Variables para la creación y edición de talleres
  tallerEdit: Taller;
  nombre: string;

  modalTallerEdit = new EventEmitter<string | MaterializeAction>();
  modalTaller = new EventEmitter<string | MaterializeAction>();

  constructor(
    private programasService: ProgramasService,
    private talleresService: TalleresService,
    private route: ActivatedRoute,
    private router: Router)
  { }

  ngOnInit() {
    //Inicializando Variables
    this.tallerEdit = new Taller;

    // Obtiene el id del programa
    this.id = this.route.snapshot.params['id'];

    this.consultaInicial();
  }

  // Método: consultaInicial
  consultaInicial(){
    // Llama al servicio
    this.programasService.ver(this.id).subscribe(
      programa => {
        this.programa = programa;
      }
    );
  }

  // Método: modificarTaller   || Objetivo: actualizar el nombre de un taller
  modificarTaller(){

    this.talleresService.actualizar(this.tallerEdit).subscribe(
      message => {
        this.consultaInicial();
        Materialize.toast("Nombre de taller actualizado", 3000);
        this.closeEdition();
      }, error => {
        Materialize.toast("Error en la actualización", 3000);
      }
    );
  }

  // Método: crearTaller     ||  Objetivo: crear un nuevo taller
  crearTaller(form: any){
    let nuevo: boolean = true;

    if(this.nombre){
      this.programa.talleres.forEach((t)=>{
        if((t.nombre).toUpperCase() == (this.nombre).toUpperCase( )) {
          nuevo = false;
        }
      });

      if(nuevo){
        this.talleresService.crear(this.id, this.nombre).subscribe(
          message => {
            form.reset();
            this.consultaInicial();
            Materialize.toast("Nuevo taller creado", 3000);
          }, error => {
            Materialize.toast("Error en la creación", 3000);
          }
        );
      } else {
        Materialize.toast("El taller ya existe", 3000);
      }

    }else
      Materialize.toast("Complete el campo de nombre", 3000);
  }

  // Métodos para el manejo de la ventana modal de edición
  openEdition(taller: Taller) {
    this.tallerEdit.id = taller.id;
    this.tallerEdit.nombre = taller.nombre;

    this.modalTallerEdit.emit({ action: "modal", params: ['open'] });
  }
  closeEdition() {
    this.tallerEdit = new Taller;
    this.modalTallerEdit.emit({ action: "modal", params: ['close'] });
  }

  // Métodos para el manejo de la ventana modal de creación
  openNew() {
    this.modalTaller.emit({ action: "modal", params: ['open'] });
  }
  closeNew() {
    this.nombre = "";
    this.modalTaller.emit({ action: "modal", params: ['close'] });
  }

}
