import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Rx';

import { ProgramasService, Programa } from './../servicios';

declare var $: any;

@Component({
  templateUrl: './programas.component.html'
})
export class ProgramasComponent implements OnInit {
  programas: Programa[];

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private presupuestosService:ProgramasService, private router: Router) {
    // Opciones de datatable
    this.dtOptions = {
      pageLength: 10,
      pagingType: 'simple_numbers',
      order: [[0, "asc"]],
      language: {
        "emptyTable": "Sin registros disponibles en la tabla",
        "info": "Mostrando de _START_ a _END_ registros de _TOTAL_",
        "infoEmpty": "",
        "infoFiltered": "(filtrados de _MAX_ totales )",
        "lengthMenu": " ",
        "search": "Buscar:",
        "zeroRecords": "Búsqueda sin resultados",
        "paginate": {
          "first": "Primero",
          "last": "Último",
          "next": "Siguiente",
          "previous": "Anterior"
        }
      }
    };
   }

  ngOnInit() {
    // Llama al servicio
    this.presupuestosService.obtener().subscribe(
      programas => {
        // Asigna los programas y refresca la tabla
        this.programas = programas;
        console.log(programas);
        this.dtTrigger.next();
      }
    );
  }

}
