export { Programa } from './programa';
export { Taller } from './taller';
export { ProgramasService } from './programas.service';
export { TalleresService } from './talleres.service';
