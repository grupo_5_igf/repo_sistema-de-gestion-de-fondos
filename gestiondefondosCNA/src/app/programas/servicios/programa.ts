/*
*Nombre de la clase: programa
*Dirección física: src/app/programas/servicios/programa.ts
**/

import { Taller } from './';

export class Programa{
  id: number;
  nombre: string;
  fechaInicio: string;
  duracion: number;
  talleres: Taller[];
}
