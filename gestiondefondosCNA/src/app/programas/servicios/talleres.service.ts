import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { CookieService } from 'ngx-cookie';
import { Taller } from './';

@Injectable()
export class TalleresService {
  baseUrl: string;
  headers: Headers;

  constructor(private http: Http, private cookieService: CookieService) {
    this.baseUrl = environment.apiPresupuestosURL;
    this.headers = new Headers({ 'Content-Type': 'application/json', 'X-AUTH-TOKEN': this.cookieService.get('token') });
  }

  // Método: crear
  // Objetivo: crear taller
  crear(idP: number, nombre:string): Observable<string>{
    let url = this.baseUrl + "taller/new";

    // Mapeando la entrada
    let q = JSON.stringify({ id_programa: { id_programa: idP }, nombre_taller: nombre });

    return this.http.post(url, q, { headers: this.headers }).map(
      // Mapeando salida
      (response: Response) => {
        let r = response.json();
        return r;
      }
    );
  }

  // Método: actualizar
  // Objetivo: modificar el nombre de un taller
  actualizar(taller: Taller): Observable<string>{
    let url = this.baseUrl + "taller/" + taller.id + "/edit";

    // Mapeando la entrada
    let q = JSON.stringify({ nombre_taller: taller.nombre });

    return this.http.put(url, q, { headers: this.headers }).map(
      // Mapeando salida
      (response: Response) => {
        let r = response.json();

        return r;
      }
    );
  }
}
