import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';

import { CookieService } from 'ngx-cookie';
import { Programa, Taller } from './';

@Injectable()
export class ProgramasService {
  baseUrl: string;
  busUrl: string;
  headers: Headers;

  constructor(private http: Http, private cookieService: CookieService) {
    this.baseUrl = environment.apiPresupuestosURL;
    this.busUrl = environment.busURL;

    this.headers = new Headers({ 'Content-Type': 'application/json', 'X-AUTH-TOKEN': this.cookieService.get('token') });
  }

  // Método: obtener
  // Objetivo: obtener listado programas disponibles
  obtener(): Observable<Programa[]>{
    //  let url = this.baseUrl + 'programa/';
    let url = this.busUrl + 'programas';

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let programas = new Array<Programa>();

        r.forEach((_programa) => {
          let programa = new Programa;
          let rf = _programa['fecha_inicio'];

          programa.id = _programa['id_programa'];
          programa.nombre = _programa['nombre_programa'];
          programa.duracion = _programa['duracion'];
          programa.fechaInicio = _programa['fecha_inicio'];

          programas.push(programa);
        });

        return programas;
      }
    );
  }

  // Método: ver
  // Objetivo: Obtener el detalle de un programa
  ver(id: number): Observable<Programa>{
    let url = this.baseUrl + 'programa/' + id;
    // let url = this.busUrl + 'programa?id=' + id;

    // Realizando GET
    return this.http.get(url, { headers: this.headers }).map(
      // Mapeando la salida
      (response: Response) => {
        let r = response.json();
        let rt = r['talleres'];

        let programa = new Programa;
        let talleres = new Array<Taller>();

        programa.nombre = r['nombre_programa'];
        programa.fechaInicio = r['fecha_inicio'];
        programa.duracion = r['duracion'];

        rt.forEach((_taller) => {
          let taller = new Taller;

          taller.id = _taller['id_taller'];
          taller.nombre = _taller['nombre_taller'];

          talleres.push(taller);
        });

        programa.talleres = talleres;

        return programa;
      }
    );
  }
}
