/*
*Nombre del módulo: programas
*Dirección física: src/app/programas/programas.module.ts
*Objetivo: Definir el módulo programas
**/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CookieModule } from 'ngx-cookie';
import { MaterializeModule } from 'angular2-materialize';
import { DataTablesModule } from 'angular-datatables';

import { ProgramasRoutingModule } from './programas-routing.module';
import { ProgramasRootComponent, ProgramasComponent, ProgramaComponent } from './componentes';
import { ProgramasService, TalleresService } from './servicios';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    MaterializeModule,
    DataTablesModule,
    CookieModule.forChild(),
    ProgramasRoutingModule
  ],
  declarations: [
    ProgramasRootComponent,
    ProgramasComponent,
    ProgramaComponent
  ],
  providers: [
    ProgramasService,
    TalleresService
  ]
})
export class ProgramasModule { }
