/*
*Nombre del módulo: programas-routing
*Dirección física: src/app/programas/programas-routing.module.ts
*Objetivo: Definir las rutas del módulo Programas
**/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgramasRootComponent, ProgramasComponent, ProgramaComponent } from './componentes';
import { AppAuthGuard } from './../login'

const routes: Routes = [
  {
    path: 'programas',
    component: ProgramasRootComponent,
    children: [
      {
        path: '',
        component: ProgramasComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Administrador"}
      },
      {
        path: ':id',
        component: ProgramaComponent,
        canActivate: [AppAuthGuard],
        data: {puesto: "Administrador"}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class ProgramasRoutingModule { }
