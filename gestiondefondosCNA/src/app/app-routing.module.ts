/*
*Nombre del módulo: app-routing
*Dirección física: src/app/app-routing.module.ts
*Objetivo: Declaración de las rutas principales
**/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports:[
    RouterModule
  ],
  declarations: [],
  providers: []
})
export class AppRoutingModule { }
